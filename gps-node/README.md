# GPS Node

GPS Node sends current GPS coordinates to topic `/gps`.

----

# Pseudocode

```
	while(ros::ok()) {
		// read data from hardware
		// pubish data to topic:/gps
	}
```