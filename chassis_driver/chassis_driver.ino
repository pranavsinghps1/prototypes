#define M_LF_MA 0
#define M_LF_MB 1
#define M_LF_EA 2
#define M_LF_EB 3

#include "Motor.h"

Motor motor_lf(M_LF_MA, M_LF_MB, M_LF_EA, M_LF_EB);
bool flag = true;

void interrupt_handler_lf(void);

void setup() {
  Serial.begin(9600);

  attachInterrupt(motor_lf.get_encoderA(), interrupt_handler_lf, FALLING);
   delay(1000);
   
  Serial.println("Initialisation Complete");
}

void loop() {

  if(flag) {
    motor_lf.start(true, 200, 7);
    delay(1500);
    motor_lf.start(false, 200, 7);
    flag = false;
  }
  
  
}

void interrupt_handler_lf() { motor_lf.tick(); }
