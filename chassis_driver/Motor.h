#ifndef Motor_h
#define Motor_h

#include "Arduino.h"

class Motor {
  private:
    byte pinA;
    byte pinB;

    byte encoderA;
    byte encoderB;
    
    int vel; // [-255, 255]
    int max_tick;
    int cur_tick;
    bool is_running;

    // TODO callibration for distance to ticks relation
    int tick_factor;
    
  public:
    Motor(byte, byte, byte, byte);
    void tick();
    void start(bool, byte, float); // direction, speed, distance
    void off();  

    // getter/setters
    int get_encoderA();
};

#endif
