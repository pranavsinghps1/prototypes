#include "Arduino.h"
#include "Motor.h"

Motor::Motor(byte pinA, byte pinB, byte encoderA, byte encoderB) {
  this->pinA = pinA;
  pinMode(pinA, OUTPUT);

  this->pinB = pinB;
  pinMode(pinB, OUTPUT);
  
  this->encoderA = encoderA;
  pinMode(encoderA, INPUT);
  
  this->encoderB = encoderB;
  pinMode(encoderB, INPUT);

  this->cur_tick = 0;
  this->max_tick = 0;
  this->is_running = false;

  // TODO callibration for distance to ticks relation
  this->tick_factor = 5;
}

/*
 *  tick() increases the motor's tick counter by 1;
 *  Stops the motor if number of required ticks(max_tick)
 *  have been completed.
 */
void Motor::tick() {

  if( digitalRead(this->encoderB) == 0 ) {
    if( digitalRead(this->encoderA) == 0 )
      this->cur_tick--;
    else
      this->cur_tick++;
  }

  if(this->cur_tick > this->max_tick)
    this->off();
}

/*
 * start() runs the motor with desired speed, for expected rotations
 * it converts distance to number of ticks needed
 * 
 * @param bool dir: true for ANTICLOCKWISE, false for CLOCKWISE
 * @param int vel: speed of motor between 0 and 255
 */
void Motor::start(bool dir, byte vel, float distance) {
    // TODO callibration for distance to ticks relation
    this->max_tick = (int) (distance * this->tick_factor);
    
    if(dir) {
      analogWrite(this->pinA, vel);
      analogWrite(this->pinB, 0);
    }
    else {
      analogWrite(this->pinA, - (int)vel);
      analogWrite(this->pinB, 0);
    }

    this->is_running = true;
}

/** off() stops the motor completely */
void Motor::off() {
  this->cur_tick = this->max_tick = 0;
  analogWrite(this->pinA, 0);
}

/** GET encoderA pin number */
int Motor::get_encoderA() { return this->encoderA; }
