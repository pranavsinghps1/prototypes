import RPi.GPIO as GPIO
import time

class vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def magnitude(self):
        return math.sqrt((self.x**2) + (self.y**2))
    
    def getAngle(self, other):
        n = self.x*other.y - other.x*self.y
        d = self.magnitude() * other.magnitude()
        return math.degrees(math.asin(n / d))

# Initialise Pi Pins
class robot:
	left_in1 = 7    # left forward
	left_in2 = 8	# left backward
	right_in1 = 11  # right forward
	right_in2 = 12  # right backward

	enable_left = 15
	enable_right = 16

	# TODO add speed of motor -> pwm

	def __init__(self, x, y, dir_vector):
		self.x = x
		self.y = y
		self.dir_vector = dir_vector

		# Initialise Pi
		GPIO.setmode(GPIO.BOARD)

		GPIO.setup(self.left_in1, GPIO.OUT)
		GPIO.setup(self.left_in2, GPIO.OUT)
		GPIO.setup(self.right_in1, GPIO.OUT)
		GPIO.setup(self.right_in2, GPIO.OUT)
		
		GPIO.setup(self.enable_left, GPIO.OUT)
		GPIO.setup(self.enable_right, GPIO.OUT)
	
	def moveTo(self, x, y):
        newDir = vector(x - self.x, y - self.y)
        theta = (self.dir_vector).getAngle(newDir)
        dist = math.sqrt( (x-self.x)**2 + (y-self.y)**2 )

        if abs(theta) != 0.0:
			print('TURN ' + str(theta) + ' degrees')
			if theta > 0:
				self.lt(theta)
			else:
				self.rt(-theta)

        if dist != 0.0:
			print('MOVE ' + str(dist) + ' units')
			self.fd(dist)

        self.dir_vector = vector(x-self.x, y-self.y)
        self.x = x
        self.y = y

	def off(self):
		GPIO.output(self.left_in1, GPIO.LOW)
		GPIO.output(self.left_in2, GPIO.LOW)
		GPIO.output(self.right_in1, GPIO.LOW)
		GPIO.output(self.right_in2, GPIO.LOW)

		GPIO.output(self.enable_left, GPIO.LOW)
		GPIO.output(self.enable_right, GPIO.LOW)

	def fd(self, dist):
		self.off()
		GPIO.output(self.enable_left, GPIO.HIGH)
		GPIO.output(self.enable_right, GPIO.HIGH)
		GPIO.output(self.left_in1, GPIO.HIGH)
		GPIO.output(self.right_in1, GPIO.HIGH)
		interval = dist*3
		time.sleep(interval) # TODO change delay
		self.off()


	def bk(self, dist):
		self.off()
		GPIO.output(self.enable_left, GPIO.HIGH)
		GPIO.output(self.enable_right, GPIO.HIGH)
		GPIO.output(self.left_in2, GPIO.HIGH)
		GPIO.output(self.right_in2, GPIO.HIGH)
		interval = dist*3
		time.sleep(interval) # TODO change delay
		self.off()

	def lt(self, dist):
		self.off()
		GPIO.output(self.enable_left, GPIO.HIGH)
		GPIO.output(self.enable_right, GPIO.HIGH)
		GPIO.output(self.left_in2, GPIO.HIGH)
		GPIO.output(self.right_in1, GPIO.HIGH)
		interval = dist*3
		time.sleep(interval) # TODO change delay
		self.off()

	def rt():
		self.off()
		GPIO.output(self.enable_left, GPIO.HIGH)
		GPIO.output(self.enable_right, GPIO.HIGH)
		GPIO.output(self.left_in1, GPIO.HIGH)
		GPIO.output(self.right_in2, GPIO.HIGH)
		interval = dist*3
		time.sleep(interval) # TODO change delay
		self.off()
	
	'''
	pwm_pin(pinList, interval, duration)
	sends pwm pulses to given pin
	
	@param pinList: pwm will be applied this list of pin-numbers
	@param interval: time difference between 2 pulses in milliseconds
	@param duration: in milliseconds
	'''
	def pwm_pin(pinList, interval, duration):
		start_time = time.time()*1000
		cur_time = time.time()*1000

		GPIO.output(pin, GPIO.LOW)
		interval = interval / 1000.0
		l = len(pinList)

		while((cur_time-start_time) <= duration):
			# high pulse
			for index in range(0, l):
				GPIO.output(pinList[index], GPIO.HIGH)
			time.sleep(interval)
			
			# low pulse
			for index in range(0, l):
				GPIO.output(pinList[index], GPIO.LOW) 
			time.sleep(interval)
			
			cur_time = time.time()*1000











