"use strict";

class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}

class Segment {

  /*
    @param length: length of arm segment
    @param color: css value of arm's color
    @param range_start: starting limit of angle in degrees
    @param range_end: ending limit of angle in degrees
  */
  constructor(length, color, range_start, range_end) {
    this.angle = range_start;
    this.length = length;
    this.color = color;
    this.range_start = range_start;
    this.range_end = range_end;
  }

  set_angle(a) {
    if(a < this.range_start)
      a = this.range_start;
    if(a > this.range_end)
      a = this.range_end;
    this.angle = a;
  }
}

$(document).ready( () => {
  
  // initialise kinematics
  const THETA = 0.5; // search for angle at THETA intervals
  const EPSILON = 1; // acceptable error when comparing 2 points

  // initialise history of boundary points
  let unreachable_history = {};
  let last_reachable;

  // initialise GUI
  const can_el = document.getElementById('screen');
  const ctx = can_el.getContext('2d');
  const screen_width = 800,
        screen_height = 600;
  ctx.translate(screen_width/2, screen_height/2);
  
  // initialise target pointer
  let target_point = null;
  const POINTER_DELTA = 2; // change target point at DELTA intervals
  
  const init_background = () => {
    // clear screen
    ctx.clearRect(-screen_width/2, -screen_height/2, screen_width, screen_height);

    // draw x-axis
    ctx.beginPath();
    ctx.moveTo(0,0);
    ctx.lineTo(screen_width/2, 0);
    ctx.strokeStyle = 'black';
    ctx.stroke();
  }

  const degToRad = deg => {
    return (deg*Math.PI)/180.0;
  }

  const draw_segment = (source_point, segment, angle) => {
    ctx.beginPath();

    angle = degToRad(angle);
    const end_point = new Point(
      source_point.x + segment.length * Math.cos(angle),
      source_point.y + segment.length * Math.sin(angle)
    );

    ctx.moveTo(source_point.x,-source_point.y);
    ctx.lineTo(end_point.x, -end_point.y);
    ctx.strokeStyle = segment.color;
    ctx.stroke();

    return end_point;
  }

  const draw_target = point => {
    init_background();
    draw_arm(segment_list);

    ctx.fillStyle = 'red';
    ctx.fillRect(point.x, -point.y, 4, 4);
    $("#mouse-x").html(point.x);
    $("#mouse-y").html(point.y);
  }

  const draw_arm = segment_list => {
    let source = new Point(0, 0),
        angle = 180;

    for(let segment of segment_list) {
      angle = angle - 180 + segment.angle;
      source = draw_segment(source, segment, angle);
    }
  }

  const get_arm_end = segment_list => {
    let source = new Point(0, 0),
        angle = 180,
        rad;

    for(let segment of segment_list) {
      angle = angle - 180 + segment.angle;
      rad = degToRad(angle);
      source = new Point(
        source.x + segment.length*Math.cos(rad),
        source.y + segment.length*Math.sin(rad)
      );
    }

    return source;
  }

  const distance_between_point = (a, b) => {
    return Math.sqrt(
      Math.pow(a.x-b.x, 2) +
      Math.pow(a.y-b.y, 2)
    );
  }

  const ik_recurse = (seg_list, seg_index, target) => {
    if(seg_index == seg_list.length) {
      const end = get_arm_end(seg_list);
      const d = distance_between_point(end, target);
      return d <= EPSILON;
    }

    // else if(seg_index == seg_list.length-1) {
    //   if(ik_recurse(seg_list, seg_index+1, target))
    //   return true;
      
    //   let end = get_arm_end(seg_list);
    //   let max_d = distance_between_point(end, target);

    //   let a = seg_list[seg_index].angle;
    //   seg_list[seg_index].set_angle(a - THETA);
    //   end = get_arm_end(seg_list);
    //   let d = distance_between_point(end, target);
    //   let dir = d<max_d ? -1 : 1;

    // }

    else {
      const start = seg_list[seg_index].range_start;
      const end = seg_list[seg_index].range_end;
      const backup = seg_list[seg_index].angle;
      let a0 = seg_list[seg_index].angle,
          a1 = seg_list[seg_index].angle + THETA;  

      while(a0>=start || a1<=end) {
        if(a0>=start) {
          seg_list[seg_index].set_angle(a0);
          if(ik_recurse(seg_list, seg_index+1, target))
            return true;
        }

        if(a1<=end) {
          seg_list[seg_index].set_angle(a1);
          if(ik_recurse(seg_list, seg_index+1, target))
            return true;
        }

        a0 -= THETA;
        a1 += THETA;
      }

      // if point is unreachable, reset original pose
      seg_list[seg_index].set_angle(backup);

      return false;
    }
  }

  const inverse_kinematics = (seg_list, target) => {
    return ik_recurse(seg_list, 0, target);
  }

  // $(document).on('mousedown', () => { MOUSE_PRESSED = true; });
  // $(document).on('mouseup', () => { MOUSE_PRESSED = false; });

  // $(document).on('keydown', event => { 
  //   if(event.keyCode == ACTIVATE_KEY_CODE) 
  //     ACTIVATE_KEY_PRESSED = true; 
  // });
  // $(document).on('keyup', event => { 
  //   if(event.keyCode == ACTIVATE_KEY_CODE)
  //     ACTIVATE_KEY_PRESSED = false;
  // });

  $(document).on('keypress', event => {
    if(event.keyCode == 119) target_point.y += POINTER_DELTA; // UP
    else if(event.keyCode == 115) target_point.y -= POINTER_DELTA; // DOWN
    else if(event.keyCode == 97) target_point.x -= POINTER_DELTA; // LEFT
    else if(event.keyCode == 100) target_point.x += POINTER_DELTA; // RIGHT
    
    const mem_key = `${target_point.x} ${target_point.y}`;
    if(unreachable_history[mem_key]) {
      console.log('UNREACHABLE', target_point);
      target_point = new Point(last_reachable.x, last_reachable.y);
      return;
    }

    const reachable = inverse_kinematics(segment_list, target_point);

    if(reachable) {
      console.log('REACHABLE', target_point);
      last_reachable = new Point(target_point.x, target_point.y);
    } else {
      console.log('UNREACHABLE', target_point);
      unreachable_history[mem_key] = true;
      target_point = new Point(last_reachable.x, last_reachable.y);
    }

    draw_target(last_reachable);
  });

  console.log('ready');
  init_background();
  let segment_list = [
    new Segment(150, 'red', 20, 120),
    new Segment(100, 'green', 20, 120),
    new Segment(75, 'blue', 20, 120)
  ];
  draw_arm(segment_list);

  target_point = get_arm_end(segment_list);
  last_reachable = new Point(target_point.x, target_point.y);
  draw_target(target_point);
});