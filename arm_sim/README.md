# Usage
* Use WASD to control target point (red dot)
* In case of unreachable points, wait for few seconds(monitor console for updates)

### Note
ik_recurse searches for possible poses by changing each arm-segment's angle by (current_angle_of_arm_segment +- n*THETA).