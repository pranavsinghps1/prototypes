import socket
import threading
import time

ROVER_IP = "127.0.0.1"
ROVER_PORT = 5005

BASE_IP = "127.0.0.1"
BASE_PORT = 5006

def start_sender():
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  
  # send messages at interval of 2 seconds
  last_time = time.time()
  while True:
    cur_time = time.time()
    if cur_time - last_time > 2:
      sock.sendto("Hello World", (ROVER_IP, ROVER_PORT))
      last_time = cur_time

def start_receiver():
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  sock.bind((BASE_IP, BASE_PORT))

  while True:
    data, addr = sock.recvfrom(1024)
    print("ROVER: " + str(data))

if __name__ == "__main__":
  sender_thread = threading.Thread(target=start_sender, args=())
  sender_thread.start()

  receiver_thread = threading.Thread(target=start_receiver, args=())  
  receiver_thread.start()

  sender_thread.join()
  receiver_thread.join()
