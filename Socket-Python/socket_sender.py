import socket
import time

UDP_IP = "127.0.0.1"
UDP_PORT = 5005
MESSAGE = "Hello, World!"

print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT
print "message:", MESSAGE

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
last_time = time.time()

while True:
  cur_time = time.time()
  if cur_time - last_time > 2:
    sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))
    last_time = cur_time