# Autonomous Node
This repository contains all code for the autonomous mode of the rover.

----

# Class
*	Leg

	Leg represents the competition destinations(legs) described in rulebook.

	* gps (float pair: x,y) : GPS coordinates of the leg
	* process (function pointer) : stuff to do on when the bot reaches this leg's GPS
	* next_leg (leg pointer) : reference to next leg; NULL if THIS is the final leg

----

# Constants
* ULTRASONIC_RANGE : minimum distance of object so the bot can move
* ANGLE0 : when there is a large object in front, the bot moves back and turns right by ANGLE0 degrees.

----

# Pseudocode

```
	cur_leg = NULL
	is_navigation_paused = false
	
	/*
		data : array of size 3, carrying data of 3 ultrasonic sensors
	*/
	handle_obstruction(data) {
		/*
			move in the direction that is >= ULTRASONIC_RANGE.
			if no direction is found, move back and take right turn: ANGLE0 degrees
		*/
	}
	
	handle_destination() {
		is_navigation_paused = true
		
		cur_leg.process()
		cur_leg = cur_leg.next
		
		is_navigation_paused = false		
		if(cur_leg == NULL) {
			 // kill current node, autonomous navigation complete
		 }
	}
	
	handle_gps(gps) {
		if(is_navigation_paused)
			return;
			
		if(gps is equal to cur_leg.gps) {
			handle_destination()
		} else {
			// maneuver according to readings
		}
	}
	
	main() {
		// initialise all nodes (create linked list)
		// initalise cur_leg
		
		subscribe('gps', handle_gps)
		subscribe('obstruction', handle_obstruction)
	}
	
```


