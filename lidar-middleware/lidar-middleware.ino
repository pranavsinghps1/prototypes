/*
 * PIN CONNECTION FOR ARDUINO UNO:
 * RED - 5V
 * BLACK - GND
 * WHITE (DATA INPUT)- 3
 * GREEN (DATA OUTPUT) - 2
 */

#include<SoftwareSerial.h>

SoftwareSerial Serial1(2, 3);

int dist;
int strength;
int check;
int uart[9];
int i;
const int HEADER = 0x59;

void setup() {
  Serial1.begin(115200);
  Serial.begin(115200);

  Serial.println("Initialization Complete.");
}

void loop() {
  if(Serial1.available()) {
    if(Serial1.read() == HEADER) {
      uart[0] = HEADER;
      if(Serial1.read() == HEADER) {
        
        uart[1] = HEADER;
        for(i=2; i<9; i++) {
          uart[i] = Serial1.read();
        }

        check = 0;
        for(i=0; i<8; i++)
          check += uart[i];

        if(uart[8] == (check&0xff)) {
          dist = uart[2] + uart[3]*256;
          strength = uart[4] + uart[5]*256;
          Serial.print("DIST = " + String(dist)+" ");
          Serial.println("STRENGTH = " + String(strength));
        }
      }
    }
  }
}
