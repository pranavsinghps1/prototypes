"""
Potential Field based path planner
author: Atsushi Sakai (@Atsushi_twi)
Ref:
https://www.cs.cmu.edu/~motionplanning/lecture/Chap4-Potential-Field_howie.pdf
"""

import numpy as np
import matplotlib.pyplot as plt
import math

# Parameters
KP = 5.0  # attractive potential gain
ETA = 100.0  # repulsive potential gain
AREA_WIDTH = 30.0  # potential area width [m]

show_animation = True

# TESTING ONLY:
# simulation: new objects
new_x = [12.0, 30.0, 32.0, 21.0, 60.0, 70.0]
new_y = [12.0, 30.0, 32.0, 45.0, 60.0, 70.0]
scan_trigger = [5, 10, 15, 20, 25, 30]

class vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def magnitude(self):
        return math.sqrt((self.x**2) + (self.y**2))
    
    def getAngle(self, other):
        n = self.x*other.y - other.x*self.y
        d = self.magnitude() * other.magnitude()
        return math.degrees(math.asin(n / d))

class bot:
    def __init__(self, x, y, dir_vector):
        self.x = x
        self.y = y
        self.dir_vector = dir_vector
    
    def moveTo(self, x, y):
        newDir = vector(x - self.x, y - self.y)
        theta = (self.dir_vector).getAngle(newDir)
        dist = math.sqrt( (x-self.x)**2 + (y-self.y)**2 )

        if abs(theta) != 0.0: print('TURN ' + str(theta) + ' degrees')
        if dist != 0.0: print('MOVE ' + str(dist) + ' units')

        self.dir_vector = vector(x-self.x, y-self.y)
        self.x = x
        self.y = y

def calc_potential_field(gx, gy, ox, oy, reso, rr):
    minx = min(ox) - AREA_WIDTH / 2.0
    miny = min(oy) - AREA_WIDTH / 2.0
    maxx = max(ox) + AREA_WIDTH / 2.0
    maxy = max(oy) + AREA_WIDTH / 2.0
    xw = int(round((maxx - minx) / reso))
    yw = int(round((maxy - miny) / reso))

    # calc each potential
    pmap = [[0.0 for i in range(yw)] for i in range(xw)]

    for ix in range(xw):
        x = ix * reso + minx

        for iy in range(yw):
            y = iy * reso + miny
            ug = calc_attractive_potential(x, y, gx, gy)
            uo = calc_repulsive_potential(x, y, ox, oy, rr)
            uf = ug + uo
            pmap[ix][iy] = uf

    return pmap, minx, miny


def calc_attractive_potential(x, y, gx, gy):
    return 0.5 * KP * np.hypot(x - gx, y - gy)


def calc_repulsive_potential(x, y, ox, oy, rr):
    # search nearest obstacle
    minid = -1
    dmin = float("inf")
    for i, _ in enumerate(ox):
        d = np.hypot(x - ox[i], y - oy[i])
        if dmin >= d:
            dmin = d
            minid = i

    # calc repulsive potential
    dq = np.hypot(x - ox[minid], y - oy[minid])

    if dq <= rr:
        if dq <= 0.1:
            dq = 0.1

        return 0.5 * ETA * (1.0 / dq - 1.0 / rr) ** 2
    else:
        return 0.0


def get_motion_model():
    # dx, dy
    motion = [[1, 0],
              [0, 1],
              [-1, 0],
              [0, -1],
              [-1, -1],
              [-1, 1],
              [1, -1],
              [1, 1]]

    return motion

"""
    potential_field_planning(sx, sy, gx, gy, ox, oy, reso, rr)
    Generates path from start point to goal point, avoding all predefined obstacles

    @param sx: start x
    @param sy: start y
    @param gx: goal x
    @param gy: goal y
    @param ox: array of x-ordinate of obstacles
    @param oy: array of y-ordinate of obstacles
    @param reso: distance b/w 2 grid cells
    @param rr: robot radius

    @param step: TESTING ONLY

    TODO
        add objection scanner
        update map
        recalculate potential field
"""
def potential_field_planning(sx, sy, gx, gy, ox, oy, reso, rr):

    # calc potential field
    pmap, minx, miny = calc_potential_field(gx, gy, ox, oy, reso, rr)

    # search path
    d = np.hypot(sx - gx, sy - gy)
    ix = round((sx - minx) / reso)
    iy = round((sy - miny) / reso)
    gix = round((gx - minx) / reso)
    giy = round((gy - miny) / reso)

    # setup initial state of robot
    initial_dir = vector(0, 1)
    robot = bot(ix, iy, initial_dir)

    if show_animation:
        draw_heatmap(pmap)
        plt.plot(ix, iy, "*k")
        plt.plot(gix, giy, "*m")

    rx, ry = [sx], [sy]
    motion = get_motion_model()
    
    # TESTING ONLY
    step = 0
    sim_obj_index = 0
    
    while d >= reso:
        # TESTING ONLY
        flag = step in scan_trigger
        if step in scan_trigger:
            # UPDATE OBJECT
            objectX = new_x[sim_obj_index]
            objectY = new_y[sim_obj_index]
            ox.append(objectX)
            oy.append(objectY)
            pmap, _, _ = calc_potential_field(gx, gy, ox, oy, reso, rr)
            sim_obj_index = sim_obj_index + 1  
            draw_heatmap(pmap)
            step = step + 1
            continue
        step = step + 1
       
        # TODO Object scanner
        # TODO Update Map

        minp = float("inf")
        minix, miniy = -1, -1

        # TODO localisation 

        for i, _ in enumerate(motion):
            inx = int(ix + motion[i][0])
            iny = int(iy + motion[i][1])
            if inx >= len(pmap) or iny >= len(pmap[0]):
                p = float("inf")  # outside area
            else:
                p = pmap[inx][iny]
            if minp > p:
                minp = p
                minix = inx
                miniy = iny

        ix = minix
        iy = miniy
        xp = ix * reso + minx
        yp = iy * reso + miny
        d = np.hypot(gx - xp, gy - yp)
        rx.append(xp)
        ry.append(yp)

        if show_animation:
            plt.plot(ix, iy, ".r")
            robot.moveTo(ix, iy)
            plt.pause(0.01)

    print("Goal!!")

    return -1


def draw_heatmap(data):
    data = np.array(data).T
    plt.pcolor(data, vmax=100.0, cmap=plt.cm.Blues)

def main():
    print("potential_field_planning start")

    sx = 0.0  # start x position [m]
    sy = 10.0  # start y positon [m]
    gx = 30.0  # goal x position [m]
    gy = 30.0  # goal y position [m]
    grid_size = 0.9 # potential grid size [m]
    robot_radius = 5.0  # robot radius [m]

    ox = [15.0, 5.0, 20.0, 25.0]  # obstacle x position list [m]
    oy = [25.0, 15.0, 26.0, 25.0]  # obstacle y position list [m]

    if show_animation:
        plt.grid(True)
        plt.axis("equal")

    # path generation
    # step = 0
    # object_index = 0
    # while True:
    #     step = potential_field_planning(sx, sy, gx, gy, ox, oy, grid_size, robot_radius, step)
    #     if step == -1:
    #         break

    #     ox.append(new_x[object_index])
    #     oy.append(new_y[object_index])
    #     object_index = object_index + 1
    #     step = step + 1
    
    potential_field_planning(sx, sy, gx, gy, ox, oy, grid_size, robot_radius)

    if show_animation:
        plt.show()


if __name__ == '__main__':
    print(__file__ + " start!!")
    main()
    print(__file__ + " Done!!")