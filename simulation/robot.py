from vector import vector
import math

class robot:
  def __init__(self, x, y, dir_vector):
    self.x = x
    self.y = y
    self.dir_vector = dir_vector
    
  def moveTo(self, x, y):
    newDir = vector(x - self.x, y - self.y)
    theta = (self.dir_vector).getAngle(newDir)
    dist = math.sqrt( (x-self.x)**2 + (y-self.y)**2 )

    if abs(theta) != 0.0: print('TURN ' + str(theta) + ' degrees')
    if dist != 0.0: print('MOVE ' + str(dist) + ' units')

    self.dir_vector = vector(x-self.x, y-self.y)
    self.x = x
    self.y = y
    
            

