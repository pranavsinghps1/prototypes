from robot import robot
from vector import vector
import numpy
import cv2
import matplotlib.pyplot as plt
import time

class cell:

    def __init__(self, row, col):
        self.row = row
        self.col = col

'''
    load initial state from file:
    Line 1: maze_height, maze_width
    Line 2: robot_row, robot_col, robot_dir_x, robot_dir_y
    Line 3: goal_row, goal_col
    Next maze_height lines contain binary strings: 
        1 means obstacle,
        0 means free
    
    @param filename: filename to read from
    @return bot, goal_row, goal_col, maze
'''
def load_environment(filename):
    f = open(filename, 'r')
    content = f.read().split('\n')
    
    line1 = content[0].split(' ')
    rows = int(line1[0])
    cols = int(line1[1])

    line2 = content[1].split(' ')
    start_row = int(line2[0])
    start_col = int(line2[1])
    dir_x = int(line2[2])
    dir_y = int(line2[3])
    
    line3 = content[2].split(' ')
    goal_row = int(line3[0])
    goal_col = int(line3[1])

    maze = []
    for x in range(3, len(content)-1):
        maze.append(list(content[x]))
    
    maze[start_row][start_col] = 'S'
    maze[goal_row][goal_col] = 'G'

    bot = cell(start_row, start_col)
    goal = cell(goal_row, goal_col)

    return bot, goal, maze

def mazeToImage(maze):
    rows = len(maze)
    cols = len(maze[0])

    a = numpy.zeros(shape=(rows, cols))
    for row in range(0, rows):
        for col in range(0, cols):
            val = maze[row][col]
            if val == '0':
                a[row][col] = 100
            elif val == '1':
                a[row][col] = 0
            elif val == 'G':
                a[row][col] = 100
            elif val == 'S':
                a[row][col] = 0
    return a

def matrix_distance(cell1, cell2):
    x = abs(cell1.row - cell2.row)
    y = abs(cell1.col - cell2.col)
    return x+y

def calculate_next_cell(bot, goal, maze):
    
    next_cell = cell(bot.row, bot.col)
    min_d = 100000000

    c = cell(bot.row-1, bot.col) # TOP
    if maze[c.row][c.col]!='1' and matrix_distance(c, goal) < min_d:
        next_cell = c
        min_d = matrix_distance(c, goal)
        # print('TOP')
    
    c = cell(bot.row-1, bot.col+1) # TOP-RIGHT
    if maze[c.row][c.col]!='1' and matrix_distance(c, goal) < min_d:
        next_cell = c
        min_d = matrix_distance(c, goal)
        # print('TOP-LEFT')
    
    c = cell(bot.row, bot.col+1) # RIGHT
    if maze[c.row][c.col]!='1' and matrix_distance(c, goal) < min_d:
        next_cell = c
        min_d = matrix_distance(c, goal)
        # print('RIGHT')
    
    c = cell(bot.row+1, bot.col+1) # BTM-RIGHT
    if maze[c.row][c.col]!='1' and matrix_distance(c, goal) < min_d:
        next_cell = c
        min_d = matrix_distance(c, goal)
        # print('BTM-RIGHT')
    
    c = cell(bot.row+1, bot.col) # BTM
    if maze[c.row][c.col]!='1' and matrix_distance(c, goal) < min_d:
        next_cell = c
        min_d = matrix_distance(c, goal)
        # print('BTM')
    
    c = cell(bot.row+1, bot.col-1) # BTM-LEFT
    if maze[c.row][c.col]!='1' and matrix_distance(c, goal) < min_d:
        next_cell = c
        min_d = matrix_distance(c, goal)
    # 
    c = cell(bot.row, bot.col-1) # LEFT
    if maze[c.row][c.col]!='1' and matrix_distance(c, goal) < matrix_distance(next_cell, goal):
        next_cell = c
    
    c = cell(bot.row-1, bot.col-1) # TOP-LEFT
    if maze[c.row][c.col]!='1' and matrix_distance(c, goal) < min_d:
        next_cell = c
        min_d = matrix_distance(c, goal)
        # print('TOP-LEFT')
    
    return next_cell
    

def autonomous_mode(bot, goal, maze):

    has_reached = False
    maze[bot.row][bot.col] = 'S'

    while has_reached == False:

        img = mazeToImage(maze)
        plt.imshow(img, cmap='gray')
        plt.show(block=False)
        time.sleep(0.2)
        plt.clf()
        plt.cla()
        plt.close()
        
        next_cell = calculate_next_cell(bot, goal, maze)
        maze[bot.row][bot.col] = '0'
        bot = next_cell
        maze[bot.row][bot.col] = 'S'   

        has_reached = bot.row == goal.row and bot.col == goal.col             

    cv2.waitKey(0)

def main():
    bot, goal, maze = load_environment('maze2')
    autonomous_mode(bot, goal, maze)

if __name__ == '__main__':
    print('START')
    main()
    print('STOP')