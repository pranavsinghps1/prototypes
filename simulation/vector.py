import math

class vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def magnitude(self):
        return math.sqrt((self.x**2) + (self.y**2))
    
    '''
        @return angle in degrees
                -ve angle means turn RIGHT
                +ve angle means turn LEFT
    '''
    def getAngle(self, other):
        dot = self.x*other.x + self.y*other.y
        det = self.x*other.y - self.y*other.x
        angle = math.degrees(math.atan2(det, dot))
        return angle