# LIDAR Node

This node sends lidar reading to topic `/lidar`. It also triggers topic `/obstruction` when LIDAR reading lies in a range.

---- 

# Pseudocode

```
	#define RANGE 5
	
	while(ros::ok()) {
		// read value
		// publish value to topic:/lidar
		// if value <= RANGE
		//     publish topic:/obstruction
	}
```