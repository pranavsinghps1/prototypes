class cell:

  def __init__(self, row, col):
    self.row = int(row)
    self.col = int(col)
  
  def __str__(self):
    return "CELL: " + str(self.row) + ", " + str(self.col)
