from cell import cell

class graph

    '''
      This graph represents a dynamic environment

      @param float MIN_NODE_DISTANCE: minimum distance that is needed between 2 nodes so they are considered "different"
    '''
  __init__(self, MIN_NODE_DISTANCE):
    self.MIN_NODE_DISTANCE = MIN_NODE_DISTANCE
    self.nodes = None # self.nodes is a KD-Tree of 'cell' type nodes

  '''
    FUNCTION addNode()
      add given node to KD-Tree and update edges

      @param cell node: node that is to be added in graph
  '''
  def addNode(self, node):
    # TODO
  
  '''
    FUNCTION deleteNode()
      delete all nodes from KD-Tree that are inside a circle with radius MIN_NODE_DISTANCE of graph
      with given node as center

      @param cell node
  '''
  def deleteNode(self, node):
    # TODO

  '''
    FUNCTION generatePath():
      Given current position and a list of destination points
      that are to be visited in the exact order,
      it returns a path of nodes to visit all nodes in destinationList.

      @param cell cur
      @param cell destinationList
  '''
  def generatePath(self, cur, destinationList):

    # TODO
    '''
      Starting with cur, iterate through destinationList:
        Find the node closest to nextNode in the destinationList
        Generate path to this node using Djkstra's algorithm
        If this closest node is not 'near' nextNode, create this destination as new node and update graph
    '''