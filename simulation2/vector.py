import math

class vector:
  def __init__(self, x, y):
    self.x = x
    self.y = y
  
  def magnitude(self):
    return math.sqrt((self.x**2) + (self.y**2))

  def unitVector(self):
    mag = self.magnitude()
    if mag == 0: mag = 1
    return vector(
      self.x / mag, self.y / mag
    )
  
  '''
    rotate
      retruns a copy of current vector
      after rotating it clockwise by given angle  
  '''
  def rotate(self, angle):
    angle = math.radians(angle)
    return vector(
      self.x * math.cos(angle) + self.y * math.sin(angle), 
      -self.x * math.sin(angle) + self.y * math.cos(angle)
    )

  '''
    getAngle
      calculates angle between given and vector and itself
      
      @return angle in degrees
              -ve angle means turn RIGHT
              +ve angle means turn LEFT
  '''
  def getAngle(self, other):
    dot = self.x*other.x + self.y*other.y
    det = self.x*other.y - self.y*other.x
    angle = math.degrees(math.atan2(det, dot))
    return angle