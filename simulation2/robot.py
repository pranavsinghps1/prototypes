from vector import vector
from cell import cell
import math

class robot:
  
  def __init__(self, label, pos, dir_vector):
    self.label = label
    self.x = pos.col
    self.y = pos.row
    self.dir_vector = dir_vector
  
  def moveTo(self, pos):
    x = pos.col
    y = pos.row
    newDir = vector(x - self.x, y - self.y)
    theta = (self.dir_vector).getAngle(newDir)
    dist = math.sqrt( (x-self.x)**2 + (y-self.y)**2 )

    if abs(theta) != 0.0: print('TURN ' + str(theta) + ' degrees')
    if dist != 0.0: print('MOVE ' + str(dist) + ' units')

    self.dir_vector = vector(x-self.x, y-self.y)
    self.x = x
    self.y = y
  
  def __str__(self):
    return 'ROBOT: ' + self.label + '\nPOS: ' + str(self.x) + ', ' + str(self.y) + '\nDIR: (' + str(self.dir_vector.x) + ', ' + str(self.dir_vector.y) + ')'