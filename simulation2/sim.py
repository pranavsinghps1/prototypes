from robot import robot
from vector import vector
from cell import cell
import numpy
import matplotlib.pyplot as plt
import time
import math

MAZE_OBSTACLE = '1'
MAZE_CLEAR = '0'
MAZE_NODE = '2'

TERRAIN_COLOR = 'white'
OBSTACLE_COLOR_DEFAULT = 'blue'
OBSTACLE_COLOR_VISIBLE = 'red'
ROVER_COLOR = 'green'
LIDAR_COLOR = 'brown'
NODE_COLOR = 'black'

LIDAR_RANGE = 4

FRAME_DELAY = 0.7

'''
  FUNCTION: loadEnvironment
    load initial state from file:
    Line 1: maze_height, maze_width
    Line 2: robot_row, robot_col, robot_dir_x, robot_dir_y
    Line 3: goal_row, goal_col
    Next maze_height lines contain binary strings: 
      1 means obstacle,
      0 means free
    
    @param filename: filename to read from
    @return bot, goal_row, goal_col, maze
'''
def loadEnvironment(filename):
  # open file
  f = open(filename, 'r')
  content = f.read().split('\n')
    
  # load maze dimensions
  line1 = content[0].split(' ')
  rows = int(line1[0])
  cols = int(line1[1])

  # load rover initial configuration
  line2 = content[1].split(' ')
  start_row = int(line2[0]) # initial coordinate : y
  start_col = int(line2[1]) # initial coordinate : x
  start = cell(start_row, start_col)
  dir_x = int(line2[2]) # initial direction vector : x
  dir_y = int(line2[3]) # initial direction vector : y
    
  # load destination point
  line3 = content[2].split(' ')
  goal_row = int(line3[0])
  goal_col = int(line3[1])
  goal = cell(goal_row, goal_col)

  # load maze
  maze = []
  for x in range(3, len(content)-1):
    maze.append(list(content[x]))
  maze[start_row][start_col] = MAZE_CLEAR
  maze[goal_row][goal_col] = MAZE_CLEAR

  return start, goal, maze

'''
  FUNCTION: initNodes
    initialises virtual nodes of map
    along x-axis, squareRoot(map_width) number of nodes are generated for each row
    distance between 2 rows is squareRoot(map_height)
  
    @param int rows: number of rows in maze
    @param int cols: number of columns in maze
    @return cell[] : list of maze cells
'''
def initNodes(rows, cols):
  cellList = []

  sqRows = math.sqrt(rows)
  sqCols = math.sqrt(cols)

  row = sqRows
  
  while row<rows:
    col = sqCols
    while col < cols:
      cellList.append( cell(row, col) )
      col += sqCols
    row += sqRows
  
  return cellList
    
'''
  FUNCTION: boundCheck
    checks if given row,col pair is inside maze
'''
def boundCheck(maze, row, col):
  if row < 0 or col < 0 or row >= len(maze) or col >= len(maze[0]):
    return False
  return True

'''
  FUNCTION: displayMaze
    plot the labrinth on matplotlib

    @param char maze[][]: 2d matrix of map
'''
def displayMaze(maze):
  row_count = len(maze)
  col_count = len(maze[0])
  x = []
  y = []

  for i in range(0, row_count):
    for j in range(0, col_count):
      if(maze[i][j] == MAZE_OBSTACLE):
        x.append(j)
        y.append(i)
  
  plt.scatter(x,y, color=OBSTACLE_COLOR_DEFAULT)

'''
  FUNCTION: displayNodes
    plots nodes on map

    @param robot bot
    @param cell[] nodeList: representing virtual nodes
'''
def displayNodes(bot, nodeList):
  x = []
  y = []

  # the nodes are overlayed on every element except bots

  for i in range(0, len(nodeList)):
    node = nodeList[i]
    if bot.y == node.row and bot.x == node.col:
      continue
    x.append(node.col)
    y.append(node.row)

  plt.scatter(x, y, color=NODE_COLOR)
    
'''
  FUNCTION: line
    draw line between given 2 cells
    it uses DDA algorithm to calculate points

    @param cell cell1
    @param cell cell2
    @param color: line color
'''
def drawLine(cell1, cell2, color):
  x1 = cell1.col
  y1 = cell1.row
  x2 = cell2.col
  y2 = cell2.row
  xRes = []
  yRes = []

  dx = x2-x1
  dy = y2-y1
  if abs(dx) > abs(dy):
    steps = abs(dx)
  else:
    steps = abs(dy)
  
  xInc = dx / float(steps)
  yInc = dy / float(steps)

  x = x1
  y = y1
  for i in range(0, steps+1):
    xRes.append(x)
    yRes.append(y)
    x += xInc
    y += yInc

  plt.scatter(xRes, yRes, color=color)

'''
  FUNCTION: drawLidarLine
    draw line between given 2 cells
    it uses DDA algorithm to calculate points

    @param maze
    @param cell cell1
    @param cell cell2
    @param color: line color
'''
def drawLidarLine(maze, cell1, cell2, color):
  x1 = cell1.col
  y1 = cell1.row
  x2 = cell2.col
  y2 = cell2.row
  xRes = []
  yRes = []

  dx = x2-x1
  dy = y2-y1
  if abs(dx) > abs(dy):
    steps = abs(dx)
  else:
    steps = abs(dy)
  
  if steps == 0:
    return
  
  xInc = dx / float(steps)
  yInc = dy / float(steps)

  x = x1
  y = y1
  for i in range(0, steps+1):
    if(maze[int(y)][int(x)] == MAZE_OBSTACLE and boundCheck(maze, int(y), int(x))):
      continue

    xRes.append(int(x))
    yRes.append(int(y))
    x += xInc
    y += yInc

  plt.scatter(xRes, yRes, color=color)

'''
  FUNCTION: getLidarEnd
    get list of cells visible by LIDAR
  @param robot bot
  @return cell[]: maze cell list
'''
def getLidarEnd(bot):
  cellList = []

  dir = bot.dir_vector
  angleList = [-45, 45]
  botCell = cell(bot.y, bot.x)
  for angle in range(-45, 45, 10):
    dirVec = dir.rotate(angle).unitVector()
    p = cell(bot.y + LIDAR_RANGE*dirVec.y, bot.x + LIDAR_RANGE*dirVec.x)
    cellList.append(p)

  return cellList

'''
  FUNCTION: drawLidarRange
    given a list of cells representing view of lidar,
    color the cells on map

    @param maze
    @param robot bot
'''
def drawLidarRange(maze, bot):
  botCell = cell(bot.y, bot.x)
  list =  getLidarEnd(bot)

  for i in range(0, len(list)):
    drawLidarLine(maze, botCell, list[i], LIDAR_COLOR)
  
  plt.scatter([bot.x], [bot.y], color=ROVER_COLOR)

'''
  FUNCTION: clearLidarRange
    given a list of cells representing view of lidar,
    clear the cells on map

    @param maze
    @param robot bot
'''
def clearLidarRange(maze, bot):
  botCell = cell(bot.y, bot.x)
  list =  getLidarEnd(bot)

  for i in range(0, len(list)):
    drawLidarLine(maze, botCell, list[i], TERRAIN_COLOR)
  
  plt.scatter([bot.x], [bot.y], color=ROVER_COLOR)

'''
  FUNCTION: moveBot
    move robot to given point on map
    
    @param maze
    @param nodeList
    @param robot bot: current robot state
    @param cell pos: new bot cell
    @return cell: updated robot cell
'''
def moveBot(maze, nodeList, bot, pos):
  if bot != None:
    # clear last lidar range
    clearLidarRange(maze, bot)

    # clear last bot position
    plt.scatter([bot.x], [bot.y], color=TERRAIN_COLOR)
  else:
    bot = robot('Rover', pos, vector(1,0))

  # change bot location
  bot.moveTo(pos)

  # update bot position on map
  plt.scatter([bot.x], [bot.y], color=ROVER_COLOR)

  # draw new lidar view
  drawLidarRange(maze, bot)

  # overlay nodes
  displayNodes(bot, nodeList)

  # update visuals
  plt.draw()
  plt.pause(FRAME_DELAY)

  return bot

'''
  FUNCTION: autonomousMotion()
    move bot through a list of cells autonomously

    @param robot bot: current rover state
    @param cell[] destinationList: destionation cells in the required order
    @param cell[] nodeList: virtual nodes of map
'''
def autonomousMotion(bot, destinationList, nodeList):
  # TODO
  return

'''
  FUNCTION: main
'''
def main():
  start, goal, maze = loadEnvironment('maze2')
  nodeList = initNodes(len(maze), len(maze[0]))

  bot = moveBot(maze, nodeList, None, start)
  bot = moveBot(maze, nodeList, bot, cell(3, 3))
  bot = moveBot(maze, nodeList, bot, cell(5, 5))
  bot = moveBot(maze, nodeList, bot, cell(8,10))

  plt.pause(20.000)

if __name__ == '__main__':
    print('START')
    main()
    print('STOP')