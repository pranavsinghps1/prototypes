#include <TimerOne.h>
const int IN1=27;
const int IN2=28;
const int ENA=22;
const int IN3=26;
const int IN4=25;
const int ENB=23;
volatile float CountA = 0;
volatile float CountD = 0;

long c;
float rpm;
float rpmD;

float kp = .07;
float ki = .05;
float kd = .03;

float error;
float lastError;
float input;
float out;
float Setpoint;
float cumError, rateError;


float errorD;
float lastErrorD;
float inputD;
float outD;
float SetpointD;
float cumErrorD, rateErrorD;

unsigned long time1;
unsigned long time2;
long timeA;
unsigned long startTime;
float i;
String str;

void setup(){
          
        Setpoint = 100;  
        SetpointD = 60; 
        pinMode(IN1, OUTPUT);
        pinMode(IN2, OUTPUT);
        pinMode(ENA, OUTPUT);
        pinMode(IN4, OUTPUT);
        pinMode(IN3, OUTPUT);
        pinMode(ENB, OUTPUT);
      
        pinMode(3, INPUT);
        pinMode(2, INPUT);
        pinMode(17,INPUT);
        pinMode(16,INPUT);
        attachInterrupt(digitalPinToInterrupt(3), EncoderA, RISING);
        attachInterrupt(digitalPinToInterrupt(17), EncoderD, RISING);
        Timer1.initialize(50000); 
        Timer1.attachInterrupt(isr);               
  
        Serial.begin(9600);
        
}      

void loop(){
  startTime = millis();
     if(Serial.available()){
        str = Serial.readStringUntil('\n');
        int str_len = str.length() + 1; 
        char char_array[str_len];
        str.toCharArray(char_array, str_len);
 
        //buf = my.toCharArray(buf, len)
        int n = sscanf(char_array, "%f,%f,%f", &kp, &kd, & ki);
          //Serial.print(F("n="));
          //Serial.println(n);
          Serial.print(F("kp="));
          Serial.println(kp);
        
          Serial.print(F("kd="));
          Serial.println(kd);
     
          Serial.print(F("ki="));
          Serial.println(ki);
    }
  
     digitalWrite(IN4,HIGH);
     digitalWrite(IN3,LOW);
     digitalWrite(IN2,LOW);
     digitalWrite(IN1,HIGH);
     //Serial.println("enter kp value:");
  
  

   }
void isr()        // interrupt service routine - tick every 0.1sec
{      time1 = micros();
      // timeA = time1 - time2;
       rpm = 60.0*(CountA/90)/0.05;  //calculate motor speed, unit is rpm
       //Serial.println(CountA);
       CountA=0;
            
       error = Setpoint - rpm; 
       cumError += error;                 
       rateError = (error - lastError);                                  
       float out = kp*error + ki*cumError + kd*rateError; 
       analogWrite(ENB,out);
       lastError = error; 
      
       rpmD = 60.0*(abs(CountD)/90)/0.05;  //calculate motor speed, unit is rpm
       CountD=0;
            
        errorD = SetpointD - rpmD; 
        cumErrorD += errorD; //* elapsedTime;                
        rateErrorD = (errorD - lastErrorD);///elapsedTime;                                  
        float outD = kp*errorD + ki*cumErrorD + kd*rateErrorD; 
        analogWrite(ENA,outD); 
        //Serial.println(outD);
        lastErrorD = errorD;
        if (rpm >= 70){
          digitalWrite(IN2,LOW);
          digitalWrite(IN1,LOW);
        }
        
        time2 = micros();
        timeA = time2-time1;
//
//       Serial.print("rpmA:");
//       Serial.println(rpm);
//       Serial.print("outA:"); 
//       Serial.println(out);
//       Serial.println();
//       Serial.print("rpmD:");
//       Serial.println(rpmD);
       Serial.print("outD:"); 
       Serial.println(outD); 

//       Serial.println(timeA);
                                
                                               
}

void EncoderA() {
  if (digitalRead(3) == HIGH) {
    if (digitalRead(2) == LOW) {
      CountA++;
    } 
    else if(digitalRead(2) == HIGH) {
      CountA--;
    }}}
    
void EncoderD() {
  if (digitalRead(17) == HIGH) {
    if (digitalRead(16) == LOW) {
      CountD++;
    } 
    else if(digitalRead(16) == HIGH) {
      CountD--;
    }}}     
    
void Motor1_Forward(int Speed)
{
     digitalWrite(IN1,HIGH);
     digitalWrite(IN2,LOW);
     analogWrite(ENA,out);
}

void Motor1_Backward(int Speed)
{
     digitalWrite(IN1,LOW);
     digitalWrite(IN2,HIGH);
     analogWrite(ENA,out);
}
void Motor1_Brake()
{
     digitalWrite(IN1,LOW);
     digitalWrite(IN2,LOW);
}
void Motor2_Forward(int Speed)
{
     digitalWrite(IN3,HIGH);
     digitalWrite(IN4,LOW);
     analogWrite(ENB,outD);
}

void Motor2_Backward(int Speed)
{
     digitalWrite(IN3,LOW);
     digitalWrite(IN4,HIGH);
     analogWrite(ENB,outD);
}
void Motor2_Brake()
{
     digitalWrite(IN3,LOW);
     digitalWrite(IN4,LOW);
}
    
