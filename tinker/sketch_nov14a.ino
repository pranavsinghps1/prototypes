#include "CytronMotorDriver.h"
CytronMD motor1(PWM_DIR, 2,3);
CytronMD motor2(PWM_DIR, 4, 5);
CytronMD motor3(PWM_DIR, 6,7);
CytronMD motor4(PWM_DIR, 8,9);
CytronMD motor5(PWM_DIR, 10,11);
CytronMD motor6(PWM_DIR, 12,13);
#define Vx A0
#define Vy A1
#define Button1 A2
int x,y,m,n;
void setup() {
 pinMode(Vx, INPUT); 
 pinMode(Vy, INPUT);
 pinMode(Button1, INPUT_PULLUP);
}

void loop() {
  m = analogRead(Vx); 
  n = analogRead(Vy);
 x=(230*(509-n))/1023;
 if(m==526)
 motor1.setSpeed(x);
 motor2.setSpeed(x);
 motor3.setSpeed(x);
 motor4.setSpeed(x);
 motor5.setSpeed(x);
 motor6.setSpeed(x);
 
}
