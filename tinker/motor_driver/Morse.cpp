#include "Arduino.h"
#include "Morse.h"

Morse::Morse(int def) {
    Serial.println("ID: " + String(def));
    this->id = def;
}

void Morse::process(char c) {
    if(c == '0')
        Serial.print(".");
    else if(c == '1')
        Serial.print("_");
    else
        Serial.print("#");
    
    delay(500);
}

int Morse::get_id() {
  return this->id;
}
