#ifndef Morse_h
#define Morse_h

#include "Arduino.h"

class Morse {
    private:
       int id;
    public:
      Morse(int);
      
      void process(char);
      int get_id();
};

#endif
