
#include "CytronMotorDriver.h"
#define Vx A0
#define Vy A1
#define Wx A3
#define Wy A4
#define Button1 A2
#define Button2 A5
CytronMD motor1(PWM_DIR, 2,3);
CytronMD motor2(PWM_DIR, 4, 5);
CytronMD motor3(PWM_DIR, 6,7);
CytronMD motor4(PWM_DIR, 8,9);
CytronMD motor5(PWM_DIR, 10,11);
CytronMD motor6(PWM_DIR, 12,13);
int x, y, btnA,w,v,btnB;
void setup()
{ 
  pinMode(Vx, INPUT); 
  pinMode(Vy, INPUT); 
  pinMode(Button1, INPUT_PULLUP);
  pinMode(Wx, INPUT); 
  pinMode(Wy, INPUT); 
  pinMode(Button2, INPUT_PULLUP);

  Serial.begin(9600);
}
void forward_halfspeed()
{
      motor1.setSpeed(110);
      motor2.setSpeed(110);
      motor3.setSpeed(110);
      motor4.setSpeed(110);
      motor5.setSpeed(110);
      motor6.setSpeed(110);  
  }
void forward_fullspeed()
{
      motor1.setSpeed(220);
      motor2.setSpeed(220);
      motor3.setSpeed(220);
      motor4.setSpeed(220);
      motor5.setSpeed(220);
      motor6.setSpeed(220);
    }
void zero_speed()
{
      motor1.setSpeed(0);
      motor2.setSpeed(0);
      motor3.setSpeed(0);
      motor4.setSpeed(0);
      motor5.setSpeed(0);
      motor6.setSpeed(0);
      
  }
 void backward_halfspeed()
{
      motor1.setSpeed(-110);
      motor2.setSpeed(-110);
      motor3.setSpeed(-110);
      motor4.setSpeed(-110);
      motor5.setSpeed(-110);
      motor6.setSpeed(-110);    
  }
 void backward_fullspeed()
 {
      motor1.setSpeed(-220);
      motor2.setSpeed(-220);
      motor3.setSpeed(-220);
      motor4.setSpeed(-220);
      motor5.setSpeed(-220);
      motor6.setSpeed(-220);
 }

void loop()
{
  x = analogRead(Vx);
  y = analogRead(Vy); 
  btnA = digitalRead(Button1);
  v = analogRead(Wx); 
  w= analogRead(Wy); 
  btnB = digitalRead(Button2);
//  Serial.print("x="); 
//  Serial.print(x);
//  Serial.print("\t\t"); 
//  Serial.print("y=");
//  Serial.print(y);  
//  Serial.print("\t\t"); 
//  Serial.print("btnA=");
//  Serial.print(btnA);
//  Serial.print("\t\t\t"); 
//  Serial.print("v="); 
//  Serial.print(v);
//  Serial.print("\t\t"); 
//  Serial.print("w=");
//  Serial.print(w);  
//  Serial.print("\t\t"); 
//  Serial.print("btnB=");
//  Serial.println(btnB); 
  if(x==526 && y==509 && v==526 && w==509)
  {
    zero_speed();
    Serial.println("zero_speed");
    }
    else if(y>509 && y<= 766 && x==526 )
    {
      backward_halfspeed();
      Serial.println("backward_halfspeed");
      }
      else if(y<=1023 && y>766  && x==526)
      {
        backward_fullspeed();
        Serial.println("backward_fullspeed");
        }
       else if(y<254 && x==526)
      {
        forward_fullspeed();
        Serial.println("forward_fullspeed");
        }
        else if(y>254 && y<509 && x==526)
      {
        forward_halfspeed();
        Serial.println("forward_halfspeed");
        }
        else
        {
          zero_speed();
          Serial.println("zero_speed");
          }
          delay(100);
}
